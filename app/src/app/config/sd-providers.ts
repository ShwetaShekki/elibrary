import { SDBaseService } from 'app/n-services/SDBaseService';
//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-addbook
import { addbook } from '../sd-services/addbook';
//CORE_REFERENCE_IMPORT-register
import { register } from '../sd-services/register';
//CORE_REFERENCE_IMPORT-login
import { login } from '../sd-services/login';

export const sdProviders = [
  SDBaseService,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-addbook
  addbook,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-register
  register,
  //CORE_REFERENCE_PUSH_TO_SD_ARRAY-login
  login
];
