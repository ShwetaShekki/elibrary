/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class authserviceService implements CanActivate {
    constructor(public router: Router) {}
    canActivate(): boolean {
    if (localStorage.getItem('token') === null) {
        console.log('checking');
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

}
