/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';

@Injectable()
export class imageserviceService {
    imageformation: any;
images = [
        {
            imgSrc: "Web/images.jpg",
            bookid:1,
            bookname:'The Intelligent Investor'
        },
        {
            bookid:2,
            bookname:'The Book Hog',
            imgSrc: "Web/image1.jpg"

        },
        {
            imgSrc: "Web/image2.jpg",
            bookid:3,
            bookname:'Magic Tree House'
        },
        {
            imgSrc: "Web/image3.jpg",
            bookid:4,
            bookname:'The Holy Book Of Hindu'
        },
        {
            imgSrc: "Web/image4.jpg",
            bookid:5,
            bookname:'Zakha and Charity'
        },
        {
            imgSrc: "Web/image5.jpg",
            bookid:6,
            bookname:'Born to Write'
        }
       
    ];
    newimages = [
        {
            imgSrc: "Web/imagenew1.jpg",
            bookid:7,
            bookname:'MAMBA MENTALITY'
        },
        {
            imgSrc: "Web/imagenew2.jpg",
            bookid:8,
            bookname:'The Secret Book'

        },
        {
            imgSrc: "Web/imagenew3.jpg",
            bookid:9,
            bookname:'The Sports Book'
        },
        {
            imgSrc: "Web/imagenew4.jpg",
            bookid:10,
            bookname:'The Boy on The Shed'
        },
        {
            imgSrc: "Web/imagenew5.jpg",
            bookid:11,
            bookname:'Professional Racing Driver'
        },
        {
            imgSrc: "Web/imagenew6.jpg",
            bookid:12,
            bookname:'Beat The Sports Book'
        },
        {
            imgSrc: "Web/imagenew8.jpg",
            bookid:13,
            bookname:'Best Soccer Players'
        }
    ];
    sidenavList = ["Home", "Dashboard", "Contact"]
    getImages() {
        return this.images;
    }
    getHomeList() {
        return this.sidenavList;
    }
    getNewimages(){
        return this.newimages;
    }
}
