/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {FormControl, FormGroup, Validators , FormBuilder} from '@angular/forms';
import { addbook } from '../../sd-services/addbook';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-addbooks',
    templateUrl: './addbooks.template.html'
})

export class addbooksComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    bookDetails: FormGroup;

    constructor(private bdms: NDataModelService, private addBookService: addbook) {
        super();
        this.mm = new ModelMethods(bdms);
    }


    uploadOptions = {
        "entityName": "users", "metadata": { "key": "abcj@gmail.com" }
    }
   
    onSuccess() {
        console.log("Succesfully uploaded!");
    }

    ngOnInit() {
        this.bookDetails = new FormGroup({
            bookid: new FormControl(),
            authername: new FormControl(),
            fileupload: new FormControl()
        });
    }

    async onAddBook(bookDetails){
        var status= (await this.addBookService.onAddBook(bookDetails)).local.result;
        console.log(status);
        console.log(this.uploadOptions);
    }




}
