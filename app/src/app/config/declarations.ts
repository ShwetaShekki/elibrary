import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { APP_INITIALIZER } from '@angular/core';
import { NDataSourceService } from '../n-services/n-dataSorce.service';
import { environment } from '../../environments/environment';
import { NMapComponent } from '../n-components/nMapComponent/n-map.component';
import { NLocaleResource } from '../n-services/n-localeResources.service';
import { NAuthGuardService } from 'neutrinos-seed-services';
import { ArtImgSrcDirective } from '../directives/artImgSrc.directive';

window['neutrinos'] = {
  environments: environment
}

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-passwordComponent
import { passwordComponent } from '../components/passwordComponent/password.component';
//CORE_REFERENCE_IMPORT-model1Component
import { model1Component } from '../components/model1Component/model1.component';
//CORE_REFERENCE_IMPORT-carouselserviceService
import { carouselserviceService } from '../services/carouselservice/carouselservice.service';
//CORE_REFERENCE_IMPORT-imageserviceService
import { imageserviceService } from '../services/imageservice/imageservice.service';
//CORE_REFERENCE_IMPORT-imagesComponent
import { imagesComponent } from '../components/imagesComponent/images.component';
//CORE_REFERENCE_IMPORT-newComponent
import { newComponent } from '../components/newComponent/new.component';
//CORE_REFERENCE_IMPORT-homepageComponent
import { homepageComponent } from '../components/homepageComponent/homepage.component';
//CORE_REFERENCE_IMPORT-addbooksComponent
import { addbooksComponent } from '../components/addbooksComponent/addbooks.component';
//CORE_REFERENCE_IMPORT-registerComponent
import { registerComponent } from '../components/registerComponent/register.component';
//CORE_REFERENCE_IMPORT-authserviceService
import { authserviceService } from '../services/authservice/authservice.service';
//CORE_REFERENCE_IMPORT-homeComponent
import { homeComponent } from '../components/homeComponent/home.component';
//CORE_REFERENCE_IMPORT-loginComponent
import { loginComponent } from '../components/loginComponent/login.component';

/**
 * Reads datasource object and injects the datasource object into window object
 * Injects the imported environment object into the window object
 *
 */
export function startupServiceFactory(startupService: NDataSourceService) {
  return () => startupService.getDataSource();
}

/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];


/**
*Entry Components for @NgModule
*/
export const appEntryComponents: any = [
  //CORE_REFERENCE_PUSH_TO_ENTRY_ARRAY
];

/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  PageNotFoundComponent,
  NMapComponent,
  ArtImgSrcDirective,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-passwordComponent
passwordComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-model1Component
model1Component,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-imagesComponent
imagesComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-newComponent
newComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homepageComponent
homepageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-addbooksComponent
addbooksComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-registerComponent
registerComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homeComponent
homeComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-loginComponent
loginComponent,

];

/**
* provider for @NgModuke
*/
export const appProviders = [
  NDataSourceService,
  NLocaleResource,
  {
    // Provider for APP_INITIALIZER
    provide: APP_INITIALIZER,
    useFactory: startupServiceFactory,
    deps: [NDataSourceService],
    multi: true
  },
  NAuthGuardService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-carouselserviceService
carouselserviceService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-imageserviceService
imageserviceService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-authserviceService
authserviceService,

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'home', component: homeComponent},{path: 'addbooks', component: addbooksComponent},{path: 'homepage', component: homepageComponent, canActivate: [authserviceService],
children: [{path: 'login', component: loginComponent},{path: 'register', component: registerComponent},{path: 'new', component: newComponent,
children: []},{path: 'image', component: imagesComponent}]},{path: 'model1', component: model1Component},{path: 'login', component: loginComponent},{path: 'password', component: passwordComponent},{path: '', redirectTo: 'login', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END
