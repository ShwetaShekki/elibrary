/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, Input } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { imageserviceService } from '../../services/imageservice/imageservice.service';
import { carouselserviceService } from '../../services/carouselservice/carouselservice.service';
import { Subscription } from 'rxjs';


/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
 *
 * Service Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-carousel',
    templateUrl: './carousel.template.html',
    styles: [`
    .icon-col-height {
        height:178px !important;
    }
    .image-data {
        margin: 0.5em
    },
    .carrow{
    // margin: 10px 10px 0 10px !important;
   }

`]
})

export class carouselComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    currentXsIndex = 0;
    splicedDataSet = [];
    dataSet;
    img;
    limitImage;
    mqSub: Subscription;

    constructor(private bdms: NDataModelService, private imgService: imageserviceService, private cService: carouselserviceService) {
        super();
        this.mm = new ModelMethods(bdms);
        this.dataSet = this.imgService.getImages();
    }


    ngOnInit() {
        this.mqSub = this.cService.numberOfImage.subscribe((numOfImg) => {
            this.limitImage = numOfImg
        });
    }

    changeDataSet(dir) {
        if (dir == 1) {
            this.dataSet.push(this.dataSet.shift());
        } else {
            let temp = [];
            temp.push(this.dataSet.pop());
            for (let i = 0; i < this.dataSet.length; ++i) {
                temp.push(this.dataSet[i]);
            }
            this.dataSet = temp;

        }
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    ngOnDestroy() {
        this.mqSub.unsubscribe();
    }

}
