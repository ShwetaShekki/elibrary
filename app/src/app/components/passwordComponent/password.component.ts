/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { login } from 'app/sd-services/login';
import { Router } from '@angular/router';


/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-password',
    templateUrl: './password.template.html'
})

export class passwordComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
     email:string;

    constructor(private bdms: NDataModelService, private loginService: login, private router: Router) {
        super();
        this.mm = new ModelMethods(bdms);
    }





//send password to email
sendpasswordtoemail(data){
    console.log(data);
    this.loginService.sendpasswordtoemail({email:data});   
    this.email='';
    alert('Password is sent to Your registered email-id');
       
}

    ngOnInit() {

    }

}