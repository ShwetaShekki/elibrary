/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { imageserviceService } from '../../services/imageservice/imageservice.service';
import { carouselserviceService } from '../../services/carouselservice/carouselservice.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-images',
    templateUrl: './images.template.html'
})

export class imagesComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
 currentXsIndex = 0;
    splicedDataSet = [];
    dataSet;
    newdataset;
    img;
    limitImage;
    mqSub: Subscription;
    constructor(private bdms: NDataModelService,private imgService: imageserviceService, private cService: carouselserviceService, private router: Router) {
        super();
        this.mm = new ModelMethods(bdms);
        this.dataSet = this.imgService.getImages();
         this.newdataset = this.imgService.getNewimages();
    }

    ngOnInit() {
            this.mqSub = this.cService.numberOfImage.subscribe((numOfImg) => {
            this.limitImage = numOfImg
        });
    }

changeDataSet(dir) {
 if (dir == 1) {
 this.dataSet.push(this.dataSet.shift());
 } else {
 let temp = [];
 temp.push(this.dataSet.pop());
 for (let i = 0; i < this.dataSet.length; ++i) {
 temp.push(this.dataSet[i]);
 }
 this.dataSet = temp;
 
 }
 }

 changenewDataSet(dir) {
 if (dir == 1) {
 this.newdataset.push(this.newdataset.shift());
 } else {
 let temp = [];
 temp.push(this.newdataset.pop());
 for (let i = 0; i < this.newdataset.length; ++i) {
 temp.push(this.newdataset[i]);
 }
 this.newdataset = temp;
 
 }
 }
  
getimage(params){
console.log(params);
this.imgService.imageformation = params;
this.router.navigate(['model1']);
}

}
