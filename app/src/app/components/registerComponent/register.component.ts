/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { register } from '../../sd-services/register';
import {FormControl, FormGroup, Validators , FormBuilder} from '@angular/forms';

/**
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

/**
* 
* Serivice Designer import Example - Service Name - HeroService
* import { HeroService } from 'app/sd-services/HeroService';
*/

@Component({
    selector: 'bh-register',
    templateUrl: './register.template.html'
})

export class registerComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    formData: FormGroup;
    submitted = false;


    constructor(private bdms: NDataModelService, private registerservice: register, private formBuilder: FormBuilder) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.formData = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required]
        });
    }
    get f() { return this.formData.controls; }

    get email() {
        return this.formData.get('email');
    }
    get name() {
        return this.formData.get('name');
    }

    //post data
    async onclick(formData) {

        console.log(formData.value.password);
        if (formData.value.password === formData.value.confirmPassword) {
            var status = (await this.registerservice.postdata(formData.value)).local.result;
            console.log(status);
        } else {
            alert('passwords should be matched');
        }
    }


}
