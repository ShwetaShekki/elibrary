/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {FormControl, FormGroup, Validators , FormBuilder} from '@angular/forms';
import { login } from '../../sd-services/login';
import { Router } from '@angular/router';

/** 
 * Service import Example :
 * import { HeroService } from '../../services/hero/hero.service';
 */

 /**
 * 
 * Serivice Designer import Example - Service Name - HeroService
 * import { HeroService } from 'app/sd-services/HeroService';
 */

@Component({
    selector: 'bh-login',
    templateUrl: './login.template.html'
})

export class loginComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    loginForm: FormGroup;
    showmsg: boolean;
   
    constructor(private bdms: NDataModelService, private loginService: login, private router: Router, private fb:FormBuilder) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.loginForm = new FormGroup({
            email:new FormControl('', Validators.email),
            password: new FormControl('', [Validators.required])
        });
    }

    async onLogin(loginForm){
        console.log(loginForm.value);
        var status=(await this.loginService.onLogin(loginForm.value)).local.output;
        console.log(status)
        if(status.code === 200) {
            localStorage.setItem('user',JSON.stringify(status.doc));
            localStorage.setItem('token',status.token)
            this.router.navigate(['home']);
        } else {
            this.showmsg = true;
        }
    }




























































get f() { return this.loginForm.controls; }
    // get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
    //     this.mm.get(dataModelName, filter, keys, sort, pagenumber, pagesize,
    //         result => {
    //             // On Success code here
    //         },
    //         error => {
    //             // Handle errors here
    //         });
    // }

    // getById(dataModelName, dataModelId) {
    //     this.mm.getById(dataModelName, dataModelId,
    //         result => {
    //             // On Success code here
    //         },
    //         error => {
    //             // Handle errors here
    //         })
    // }

    // put(dataModelName, dataModelObject) {
    //     this.mm.put(dataModelName, dataModelObject,
    //         result => {
    //             // On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }

    // validatePut(formObj, dataModelName, dataModelObject) {
    //     this.mm.validatePut(formObj, dataModelName, dataModelObject,
    //         result => {
    //             // On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }

    // update(dataModelName, update, filter, options) {
    //     const updateObject = {
    //         update: update,
    //         filter: filter,
    //         options: options
    //     };
    //     this.mm.update(dataModelName, updateObject,
    //         result => {
    //             //  On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }

    // delete (dataModelName, filter) {
    //     this.mm.delete(dataModelName, filter,
    //         result => {
    //             // On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }

    // deleteById(dataModelName, dataModelId) {
    //     this.mm.deleteById(dataModelName, dataModelId,
    //         result => {
    //             // On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }

    // updateById(dataModelName, dataModelId, dataModelObj) {
    //     this.mm.updateById(dataModelName, dataModelId, dataModelObj,
    //         result => {
    //             // On Success code here
    //         }, error => {
    //             // Handle errors here
    //         })
    // }


}
